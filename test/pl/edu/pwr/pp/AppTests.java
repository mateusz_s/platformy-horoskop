package pl.edu.pwr.pp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.powermock.reflect.Whitebox;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.BufferedReader;
import java.io.PrintStream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.startsWith;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.*;
import static org.powermock.api.support.membermodification.MemberMatcher.method;


@RunWith(PowerMockRunner.class)
@PrepareForTest(App.class)
public class AppTests {
    @Test
    public void runShouldExitWhenUserChoose2() throws Exception {
        BufferedReader mockedBufferedRead = mock(BufferedReader.class);
        when(mockedBufferedRead.readLine()).thenReturn("2");
        PowerMockito.whenNew(BufferedReader.class).withAnyArguments().thenReturn(mockedBufferedRead);
        App app = new App();

        int exitCode = app.run();

        assertThat(exitCode, is(equalTo(0)));
    }

    @Test
    public void runShouldPrintMenuAndGetHoroscopeWhenUserChoose1() throws Exception {
        BufferedReader mockedBufferedRead = mock(BufferedReader.class);
        App mockedApp = mock(App.class);
        when(mockedApp.run()).thenCallRealMethod();
        when(mockedBufferedRead.readLine()).thenReturn("1", "2");
        PowerMockito.whenNew(BufferedReader.class).withAnyArguments().thenReturn(mockedBufferedRead);

        int exitCode = mockedApp.run();

        assertThat(exitCode, is(equalTo(0)));
        verifyPrivate(mockedApp, times(2)).invoke("printMenu");
        verifyPrivate(mockedApp).invoke("getHoroscopeForNextPerson", Mockito.any());
    }

    @Test
    public void runShouldPrintStatementWhenUserChooseWrongOption() throws Exception {
        final String UNKNOWN_OPTION_STATEMENT = "Nierozpoznana opcja";
        BufferedReader mockedBufferedRead = mock(BufferedReader.class);
        PrintStream mockedOut = mock(PrintStream.class);
        App mockedApp = mock(App.class);
        when(mockedApp.run()).thenCallRealMethod();
        System.setOut(mockedOut);
        when(mockedBufferedRead.readLine()).thenReturn("-1", "2");
        PowerMockito.whenNew(BufferedReader.class).withAnyArguments().thenReturn(mockedBufferedRead);

        mockedApp.run();

        verify(mockedOut).println(UNKNOWN_OPTION_STATEMENT);
    }

    @Test
    public void printMenuStructureTest() throws Exception {
        final String UNDERSCORE = "\n================================";
        final String OPTION_1 = "\t1. Nowy horoskop";
        final String OPTION_2 = "\t2. Zakończ";
        final String CHOOSE = "Twój wybór: ";

        BufferedReader mockedBufferedRead = mock(BufferedReader.class);
        PrintStream mockedOut = mock(PrintStream.class);
        System.setOut(mockedOut);
        when(mockedBufferedRead.readLine()).thenReturn("2");
        PowerMockito.whenNew(BufferedReader.class).withAnyArguments().thenReturn(mockedBufferedRead);

        App app = new App();
        Whitebox.invokeMethod(app, "printMenu");

        InOrder inOrder = inOrder(mockedOut);
        inOrder.verify(mockedOut).println(UNDERSCORE);
        inOrder.verify(mockedOut).println(OPTION_1);
        inOrder.verify(mockedOut).println(OPTION_2);
        inOrder.verify(mockedOut).println(CHOOSE);
    }

    @Test
    public void getHoroscopeForNextPersonSequence() throws Exception {
        final String NAME_QUESTION = "Podaj imię: ";
        final String SURNAME_QUESTION = "Podaj nazwisko: ";
        final String NAME = "Stefan";
        final String SURNAME = "Kowalski";
        final String HOROSCOPE_OUT = "Horoskop dla: ";
        final String HEALTH_OUT = "Zdrowie: ";
        final String LOVE_OUT = "Miłość: ";
        final String WORK_OUT = "Praca: ";

        BufferedReader mockedBufferedRead = mock(BufferedReader.class);
        PrintStream mockedOut = mock(PrintStream.class);
        System.setOut(mockedOut);
        when(mockedBufferedRead.readLine()).thenReturn(NAME, SURNAME);
        PowerMockito.whenNew(BufferedReader.class).withAnyArguments().thenReturn(mockedBufferedRead);

        App app = new App();
        Whitebox.invokeMethod(app, "getHoroscopeForNextPerson", mockedBufferedRead);

        InOrder inOrder = inOrder(mockedOut);

        inOrder.verify(mockedOut).println(NAME_QUESTION);
        inOrder.verify(mockedOut).println(SURNAME_QUESTION);
        inOrder.verify(mockedOut).println(HOROSCOPE_OUT + NAME + " " + SURNAME);
        inOrder.verify(mockedOut).println(startsWith(HEALTH_OUT));
        inOrder.verify(mockedOut).println(startsWith(LOVE_OUT));
        inOrder.verify(mockedOut).println(startsWith(WORK_OUT));
    }
}
