package pl.edu.pwr.pp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.*;


@RunWith(PowerMockRunner.class)
@PrepareForTest({GUI.class, Main.class})
public class MainTests {
    @Test
    public void mainShouldExecuteApp() throws Exception {
        App mockedApp = mock(App.class);
        String args[] = new String[0];
        whenNew(App.class).withNoArguments().thenReturn(mockedApp);

        Main.main(args);

        verify(mockedApp, times(1)).run();
    }

    @Test
    public void mainShouldExecuteGuiWhenFlagIsProvided() throws Exception {
        PowerMockito.mockStatic(GUI.class);
        String args[] = new String[1];
        args[0] = "--gui";

        doNothing().when(GUI.class, "runGui", Mockito.any());

        Main.main(args);

        verifyStatic(Mockito.times(1));
        GUI.runGui(Mockito.any());
    }
}
