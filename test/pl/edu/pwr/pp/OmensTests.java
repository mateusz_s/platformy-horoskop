package pl.edu.pwr.pp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


@RunWith(PowerMockRunner.class)
@PrepareForTest(Omens.class)

public class OmensTests {
    @Test
    public void omensCreation() throws Exception {
        final String HEALTH_OMEN = "HEALTH";
        final String LOVE_OMEN = "LOVE";
        final String WORK_OMEN = "WORK";

        Omens omens = new Omens(HEALTH_OMEN, LOVE_OMEN, WORK_OMEN);

        assertThat(omens.health, is(equalTo(HEALTH_OMEN)));
        assertThat(omens.love, is(equalTo(LOVE_OMEN)));
        assertThat(omens.work, is(equalTo(WORK_OMEN)));
    }
}
