package pl.edu.pwr.pp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.time.LocalDate;
import java.util.Date;
import java.util.Random;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.*;


@RunWith(PowerMockRunner.class)
@PrepareForTest(Horoscope.class)

public class HoroskopTests {
    final String NAME = "Janek";
    final String SURNAME = "KowalewskiAA";

    @Test
    public void ifName17ReturnHealth5() {
        Horoscope horoscope = new Horoscope(NAME, SURNAME);

        assertThat(horoscope.getHoroscope().health, is(equalTo("Postaraj się o właściwy wypoczynek.")));
    }

    @Test
    public void ifDay21ReturnLove8() throws Exception {
        Horoscope mockedHoroscope = mock(Horoscope.class);
        when(mockedHoroscope, "getDay").thenReturn(21);
        when(mockedHoroscope.getHoroscope()).thenCallRealMethod();

        assertThat(mockedHoroscope.getHoroscope().love, is(equalTo("Znajdziesz się w centrum zainteresowania.")));
        verifyPrivate(mockedHoroscope).invoke("getDay");
    }

    @Test
    public void ifRand3ReturnWork3() throws Exception {
        final int MAX_RANDOM = 12;
        Random mockedRandom = mock(Random.class);
        when(mockedRandom.nextInt(MAX_RANDOM)).thenReturn(3);
        PowerMockito.whenNew(Random.class).withNoArguments().thenReturn(mockedRandom);

        Horoscope horoscope = new Horoscope(NAME, SURNAME);

        assertThat(horoscope.getHoroscope().work, is(equalTo("Znajdź trochę czasu dla siebie.")));
        verify(mockedRandom).nextInt(MAX_RANDOM);
    }

    @Test
    public void createNewHoroscope() throws Exception {
        final int MAX_RANDOM = 12;
        final int RAND_NUMBER = 4;
        Random mockedRandom = mock(Random.class);
        when(mockedRandom.nextInt(MAX_RANDOM)).thenReturn(RAND_NUMBER);
        PowerMockito.whenNew(Random.class).withNoArguments().thenReturn(mockedRandom);

        Horoscope horoscope = new Horoscope(NAME, SURNAME);

        int randNumber = Whitebox.getInternalState(horoscope, "randNum");
        String name = Whitebox.getInternalState(horoscope, "name");
        String surname = Whitebox.getInternalState(horoscope, "surname");

        assertThat(randNumber, is(equalTo(RAND_NUMBER)));
        assertThat(name, is(equalTo(NAME)));
        assertThat(surname, is(equalTo(SURNAME)));
        verify(mockedRandom).nextInt(MAX_RANDOM);
    }

    @Test
    public void getDayShouldReturnCurrentDay() throws Exception {
        LocalDate date = LocalDate.of(2015,11,2);
        PowerMockito.mockStatic(LocalDate.class);
        when(LocalDate.now()).thenReturn(date);

        Horoscope horoscope = new Horoscope(NAME, SURNAME);

        int day = Whitebox.invokeMethod(horoscope, "getDay");
        assertThat(day, is(equalTo(date.getDayOfMonth())));
    }

    @Test
    public void nameLength() throws Exception {
        Horoscope horoscope = new Horoscope(NAME, SURNAME);

        int nameLength = Whitebox.invokeMethod(horoscope, "nameLength");
        assertThat(nameLength, is(equalTo(new String(NAME + SURNAME).length())));
    }
}
