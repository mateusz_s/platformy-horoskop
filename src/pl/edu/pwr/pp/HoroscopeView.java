package pl.edu.pwr.pp;

import javax.swing.*;
import java.awt.event.*;

public class HoroscopeView extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JLabel nameLabel;
    private JTextPane workText;
    private JTextPane loveText;
    private JTextPane healthText;

    public HoroscopeView(String name, Omens omens) {
        nameLabel.setText(name);
        workText.setText(omens.work);
        loveText.setText(omens.love);
        healthText.setText(omens.health);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });


// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
// add your code here
        this.setVisible(false);
    }

    private void onCancel() {
// add your code here if necessary
        this.setVisible(false);
    }

    public static void showHoroscope(String name, Omens omens) {
        HoroscopeView dialog = new HoroscopeView(name, omens);
        dialog.pack();
        dialog.setVisible(true);
    }
}
