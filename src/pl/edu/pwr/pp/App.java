package pl.edu.pwr.pp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class App {
    public int run() {
        System.out.println("Generator horoskopów");
        try (BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in))) {
            while(true) {
                printMenu();
                String option = bufferRead.readLine();
                if (option.equals("1")) {
                    getHoroscopeForNextPerson(bufferRead);
                } else if (option.equals("2")) {
                    return 0;
                } else {
                    System.out.println("Nierozpoznana opcja");
                }
            }
        }
        catch(IOException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    private void printMenu() {
        System.out.println("\n================================");
        System.out.println("\t1. Nowy horoskop");
        System.out.println("\t2. Zakończ");
        System.out.println("Twój wybór: ");
    }
    private void getHoroscopeForNextPerson(BufferedReader stdin) throws IOException {
        System.out.println("Podaj imię: ");
        String name = stdin.readLine();
        System.out.println("Podaj nazwisko: ");
        String surname = stdin.readLine();

        Horoscope horoscope = new Horoscope(name, surname);

        System.out.println("Horoskop dla: " + name + " " + surname);
        System.out.println("Zdrowie: " + horoscope.getHoroscope().health);
        System.out.println("Miłość: " + horoscope.getHoroscope().love);
        System.out.println("Praca: " + horoscope.getHoroscope().work);
    }

}
