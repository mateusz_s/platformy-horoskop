package pl.edu.pwr.pp;

import java.time.LocalDate;
import java.util.Random;

public class Horoscope {

	private String name;
	private String surname;
	private int randNum;
	
	public Horoscope(String _name, String _surname)
	{
		name = _name;
		surname = _surname;

		Random generator = new Random();
		randNum = generator.nextInt(12);
	}
	
	public Omens getHoroscope()
	{
		String health = Przepowiednie.ZDROWIE[nameLength() % 12];
		int day = getDay();
		String love = Przepowiednie.MILOSC[(day - 1) % 12];
		String work = Przepowiednie.PRACA[randNum];

		Omens omens = new Omens(health, love, work);
		
		return omens;
	}
	
	private int getDay()
	{
		LocalDate today = LocalDate.now();
		return today.getDayOfMonth();
	}
	
	private int nameLength()
	{
		String tmp = new String(name + surname);
		return tmp.length();
	}
}