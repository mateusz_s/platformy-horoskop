package pl.edu.pwr.pp;

public class Main {
    public static void main(String[] args) {
        if(args.length > 0 && args[0].equals("--gui")) {
            GUI.runGui(args);
        } else {
            App app = new App();
            app.run();
        }
    }
}