package pl.edu.pwr.pp;

public class Omens {
	public String health;
	public String love;
	public String work;
	
	public Omens(String _health, String _love, String _work)
	{
		health = _health;
		love = _love;
		work = _work;
	}
}