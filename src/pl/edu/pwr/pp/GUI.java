package pl.edu.pwr.pp;

import javax.swing.*;
import java.awt.event.*;

public class GUI extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField nameField;
    private JTextField surnameField;

    public GUI() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onExit();
            }
        });
    }
    private void onOK() {
// add your code here
        Horoscope horoscope = new Horoscope(nameField.getText(), surnameField.getText());
        HoroscopeView.showHoroscope(nameField.getText() + " " + surnameField.getText(), horoscope.getHoroscope());
    }

    private void onCancel() {
// add your code here if necessary
        nameField.setText("");
        surnameField.setText("");
    }

    private void onExit() {
        dispose();
    }
    public static void runGui(String[] args) {
        GUI dialog = new GUI();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
